# Copyright (C) 2018 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# This file lists projects with optional hosttests and unittests to run for
# those projects in the following format:
# [
#     # Add projects to, or remove projects from, list of projects to build.
#     # Does not affect builds where the project list is passed on the command
#     # line.
#     build(
#         # List of projects that this build entry applies to. Required.
#         projects=[
#             "<project>",
#             ...
#         ]
#         # If enabled is set to True or ommitted, add projects to list of
#         # projects to build. If enabled is set to False, remove projects from
#         # list of projects to build.
#         enabled=<True|False>
#     ),
#     ...
#     # Specify tests to run for specific projects.
#     testmap(
#         # List of projects that this testmap entry applies to. Required.
#         projects=[
#             "<project>",
#             ...
#         ]
#         # List of host-tests to run. Optional
#         tests=[
#             # Run a program on the host
#             hosttest("some_host_binary"),
#             # Use test-runner to activate a Trusty IPC port
#             boottest("port.under.test"),
#             # Use Android to activate a Trusty IPC port
#             androidport("port.under.test"),
#             # Run a shell command inside Android
#             androidtest(name="test_name", command="command to run"),
#             ...
#         ],
#     ),
#     ...
#     # Include another configuration file. If optional is True, and the file
#     # does not exist, the include statement is ignored.
#     include(<file>, optional=<True|False>),
# ]

[
    testmap(
        projects=[
            "generic-arm64-test-debug",
        ],
        tests=[
            hosttest("avb_test"),
            hosttest("keymaster_test"),
            hosttest("mock_storage_test"),
            hosttest("storage_test"),
            hosttests([
                include("trusty/kernel/build-config-kerneltests"),
                include("trusty/user/base/build-config-usertests"),
            ]),
        ],
    ),
    include("../../proprietary/scripts/build-config", optional=True),
    testmap(
        projects=[
            "qemu-generic-arm32-gicv3-test-debug",
            "qemu-generic-arm32-test-debug",
            "qemu-generic-arm64-gicv3-test-debug",
            "qemu-generic-arm64-test-debug",
            "qemu-generic-arm64u32-test-debug",
        ],
        tests=[
            # Boot tests (test-runner + kernel only)
            boottests([
                include("trusty/kernel/build-config-kerneltests"),
                include("trusty/user/base/build-config-usertests"),
            ]),
            # Android shell port tests - same as above, but triggered with
            # Linux + android userspace present
            androidporttests([
                include("trusty/kernel/build-config-kerneltests"),
                include("trusty/user/base/build-config-usertests"),
            ]),

            # Trusty linux driver tests. Unbind and bind to trigger remove and
            # probe function.
            androidtest(name="irq-driver",
                        command="echo 'trusty:irq' >"
                                "/sys/bus/platform/drivers/trusty-irq/unbind"
                                "&&"
                                "echo 'trusty:irq' >"
                                "/sys/bus/platform/drivers/trusty-irq/bind"),

            androidtest(name="log-driver",
                        command="echo 'trusty:log' >"
                                "/sys/bus/platform/drivers/trusty-log/unbind"
                                "&&"
                                "echo 'trusty:log' >"
                                "/sys/bus/platform/drivers/trusty-log/bind"),

            androidtest(name="virtio-driver",
                        # virtio remove currently hangs (bug: 142275662).
                        # Disable test until fixed
                        enabled=False,
                        command="echo 'trusty:virtio' >"
                                "/sys/bus/platform/drivers/trusty-virtio/unbind"
                                "&&"
                                "echo 'trusty:virtio' >"
                                "/sys/bus/platform/drivers/trusty-virtio/bind"),

            androidtest(name="trusty-driver",
                        # virtio remove currently hangs (bug: 142275662).
                        # Disable affected test until fixed
                        enabled=False,
                        command="echo trusty >"
                                "/sys/bus/platform/drivers/trusty/unbind"
                                "&&"
                                "echo trusty >"
                                "/sys/bus/platform/drivers/trusty/bind"),

            # stdcall test with shared memory buffers.
            # Each test run takes up to 4 arguments:
            # <obj_size>,<obj_count=1>,<repeat_share=1>,<repeat_access=3>
            #
            # Test single 4K shared memory object.
            # Test sharing 2 4K objects 100 times without accessing it.
            # Test 10 4K shared memory objects, shared 10 times, each accessed
            # 10 times.
            androidtest(name="stdcalltest",
                        command="echo '0x1000 "
                                "0x1000,2,100,0 "
                                "0x1000,10,10,10' >"
                                "'/sys/devices/platform/trusty/"
                                "trusty:test/trusty_test_run'"),

            # TIPC tests
            androidtest(name="tipc:ta2ta",
                        command="/data/nativetest64/tipc-test/tipc-test "
                                "-t ta2ta-ipc"),

            # TIPC linux to trusty echo test
            androidtest(name="tipc:echo",
                        command="/data/nativetest64/tipc-test/tipc-test "
                                "-t echo -r 100"),

            # Storage proxy restart test. The keymaster storage wrapper keeps a
            # persistent connection to the storage server. Test that this
            # connection gets re-established when the storage proxy restarts.
            androidtest(name="storage-proxy-restart",
                        command="/data/nativetest64/trusty-ut-ctrl/"
                                "trusty-ut-ctrl "
                                "com.android.keymaster-unittest"
                                "&&"
                                "stop storageproxyd"
                                "&&"
                                "killall storageproxyd"
                                "&&"
                                "("
                                "sleep 5"
                                "&&"
                                "start storageproxyd"
                                "&"
                                "/data/nativetest64/trusty-ut-ctrl/"
                                "trusty-ut-ctrl "
                                "com.android.keymaster-unittest"
                                "&&"
                                "wait"
                                ")"
                                "&&"
                                "/data/nativetest64/trusty-ut-ctrl/"
                                "trusty-ut-ctrl "
                                "com.android.keymaster-unittest"),

            # VTS
            androidtest(name="vts:gatekeeper@1.0",
                        command="/data/nativetest64/"
                                "VtsHalGatekeeperV1_0TargetTest/"
                                "VtsHalGatekeeperV1_0TargetTest"),
            # VerificationTokenTest.TestCreation and
            # VerificationTokenTest.MacChangesOnChangingTimestamp *should* pass,
            # but do timing measurements such that the emulator won't always
            # pass.
            #
            # These tests should pass *most* of the time on the emulator, but
            # are too flaky to have on in presubmit.
            #
            # If we test on real devices, these tests should be *on*.
	    #
	    # EncryptionOperationsTest.AesWrongPurpose is tempoarily excluded
	    # from testing due to pending modification on vts:keymaster@4.0.
	    # http://b/146940505
	    #
	    # AttestationTest.RsaAttestation, AttestationTest.EcAttestation,
	    # and AttestationTest.AttestationApplicationIDLengthProperlyEncoded
	    # are excluded from testing,
	    # since attestation is not set on the emulator.
            androidtest(name="vts:keymaster@4.0",
                        command="/data/nativetest64/"
                                "VtsHalKeymasterV4_0TargetTest/"
                                "VtsHalKeymasterV4_0TargetTest"
                                " --gtest_filter="
                                "\"-*/VerificationTokenTest.TestCreation/*:"
                                "*/VerificationTokenTest."
                                "MacChangesOnChangingTimestamp/*:"
                                "*/AttestationTest.RsaAttestation/*:"
                                "*/AttestationTest.EcAttestation/*:"
                                "*/AttestationTest.AttestationApplication"
                                "IDLengthProperlyEncoded/*\"",
                        timeout=(60 * 20)),
        ],
    ),
]
